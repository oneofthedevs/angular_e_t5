import { Component, Host, OnInit, Optional } from '@angular/core';
import { SwapiService } from 'src/app/services/swapi/swapi.service';

@Component({
  selector: 'child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.scss'],
  providers: [SwapiService],
})
export class ChildComponent implements OnInit {
  public characterData: any;
  constructor(@Host() @Optional() private _swapi: SwapiService) {}

  ngOnInit(): void {
    this.GetCharatetData();
  }

  private GetCharatetData(): void {
    const id: number = this.getRandomNumber();
    this._swapi
      .getCharacterData(id)
      .subscribe((res) => (this.characterData = res));
  }

  private getRandomNumber(): number {
    return Math.floor(Math.random() * 70);
  }
}
