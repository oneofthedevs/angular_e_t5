import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ParentComponent } from './components/parent/parent.component';
import { ChildComponent } from './components/parent/child/child.component';
import { HttpClientModule } from '@angular/common/http';
import { BoredApiService } from './services/boredApi/bored-api.service';
import { boredApiProvider } from './extra/InjectionToken.class';
import { InternalService } from './services/internal/internal.service';
import { SwapiService } from './services/swapi/swapi.service';

@NgModule({
  declarations: [AppComponent, ParentComponent, ChildComponent],
  imports: [BrowserModule, AppRoutingModule, HttpClientModule],
  providers: [
    {
      provide: boredApiProvider,
      useClass: BoredApiService,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
