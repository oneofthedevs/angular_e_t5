import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { boredApi } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable()
export class BoredApiService {
  private url: string = boredApi.baseURL;

  constructor(private _http: HttpClient) {}

  public getRandomIdea(): Observable<any> {
    return this._http
      .get(`${this.url}${boredApi.activity}?participants=1`)
      .pipe(map((x: any) => x.activity));
  }
}
