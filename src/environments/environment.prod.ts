export const environment = {
  production: true,
};

export const boredApi = {
  baseURL: 'https://www.boredapi.com/api/',
  activity: 'activity/',
};

export const swapi = {
  baseURL: 'https://swapi.dev/api/',
  people: 'people/',
  planets: 'planets/',
  starships: 'starships/',
};
